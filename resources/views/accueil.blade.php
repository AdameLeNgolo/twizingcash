<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link rel="stylesheet" href="{{ asset('/css/master.css') }}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    
    <title> {{ config('app.name') }} </title>

    {{-- Font awesome --}}
  <script src="https://kit.fontawesome.com/f794276008.js" crossorigin="anonymous"></script>

  </head>
  <body>
      
      {{-- Navbar --}}
        
        <nav class="navbar navbar-expand-lg navbar-light">
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
            <a class="navbar-brand" href="#">{{ config('app.name') }}</a>
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
              <li class="nav-item active">
                <a class="nav-link" href="#">Accueil <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">A Propos</a>
              </li>

              <li class="nav-item">
                <a class="nav-link" href="#">Nos Partenaires</a>
              </li>

              <li class="nav-item">
                <a class="nav-link" href="#">Notre Equipe</a>
              </li>

              <li class="nav-item">
                <a class="nav-link" href="#">Nos Contacts</a>
              </li>
            </ul>

            <ul class="lang">
              <li>
                <a href="#">Fr <img src="{{ asset('/image/fr.png') }}" alt=""></a>
              </li>
              <li>
                <a href="#">En <img src="{{ asset('/image/en.png') }}" alt=""></a>
              </li><li>
                <a href="#">Es <img src="{{ asset('/image/es.svg') }}" alt=""></a>
              </li>
            </ul>

            {{-- <form class="form-inline my-2 my-lg-0">
              <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
              <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form> --}}
          </div>
        </nav>

      {{-- end Navbar --}}

      {{-- Caroussel --}}

        <div class="bd-example" >
          <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
              <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
              <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
              <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
              <div class="carousel-item active">
                <img src="{{ asset('/image/bg2.jpg') }}" alt="..." style="">
                <div class="carousel-caption d-none d-md-block">
                  <h5>First slide label</h5>
                  <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                </div>
              </div>
              <div class="carousel-item">
                <img src="{{ asset('/image/bg3.jpg') }}" alt="...">
                <div class="carousel-caption d-none d-md-block">
                  <h5>Second slide label</h5>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </div>
              </div>
              <div class="carousel-item">
                <img src="{{ asset('/image/bg4.jpg') }}" alt="...">
                <div class="carousel-caption d-none d-md-block">
                  <h5>Third slide label</h5>
                  <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
                </div>
              </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
        </div>

      {{-- End Caroussel --}}

    {{-- main --}}
        
        <main>
            <div id="about">
                <div class="float-left left">
                    <img src="{{ asset('/image/logo.jpeg') }}" alt="">
                </div>
                <div class="right">
                    <h4>{{ config('app.name') }}</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa, quia enim. Doloribus possimus quas eum, magnam nemo cumque nostrum reprehenderit, assumenda alias repellendus ut dolorum sed rem dolore inventore voluptatum.</p>

                    <a href="#"> Nous Contacter <i class="fas fa-hand-point-down"></i> </a>
                </div>
            </div>

            <div id="mission" >
                <h4 class="mb-4">Notre Mission</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur at nihil officiis vel necessitatibus, cum minus velit fugiat? Illo, sint! Ex praesentium vel, sed voluptate debitis aliquid explicabo, veritatis reprehenderit!</p>

                <div class="card-missions">
                    <div class="card-mission">
                        <h5>Mission 1</h5>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rerum modi, saepe minima error dolorum enim molestias esse culpa, doloremque ea similique provident accusantium blanditiis, facilis earum. Ullam animi sed, ea.</p>
                    </div>
                    <div class="card-mission">
                        <h5>Mission 1</h5>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rerum modi, saepe minima error dolorum enim molestias esse culpa, doloremque ea similique provident accusantium blanditiis, facilis earum. Ullam animi sed, ea.</p>
                    </div>
                    <div class="card-mission">
                        <h5>Mission 1</h5>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rerum modi, saepe minima error dolorum enim molestias esse culpa, doloremque ea similique provident accusantium blanditiis, facilis earum. Ullam animi sed, ea.</p>
                    </div>
                    
                </div>
            </div>

            <div id="partenaire">
                <h4 class="mb-4">Nos Partenaires</h4>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis non harum molestiae sit impedit unde suscipit exercitationem nulla, quis, in repellat fuga eligendi sed culpa quam, excepturi velit! Quibusdam, commodi.
                </p>
                <div class="cards-part">
                    <div class="card-part">
                        <img src="partner/Auchan.svg.png" alt="">
                        {{-- <h5>Auchan</h5> --}}
                    </div>
                    <div class="card-part">
                        <img src="partner/auchan.jpg" alt="">
                        {{-- <h5>Auchan</h5> --}}
                    </div>
                    <div class="card-part">
                        <img src="partner/Carrefour.svg" alt="">
                        {{-- <h5>Auchan</h5> --}}
                    </div>
                    <div class="card-part">
                        <img src="partner/auchan.jpg" alt="">
                        {{-- <h5>Auchan</h5> --}}
                    </div>
                    <div class="card-part">
                        <img src="partner/auchan.jpg" alt="">
                        {{-- <h5>Auchan</h5> --}}
                    </div>
                    <div class="card-part">
                        <img src="partner/auchan.jpg" alt="">
                        {{-- <h5>Auchan</h5> --}}
                    </div>
                    <div class="card-part">
                        <img src="partner/auchan.jpg" alt="">
                        {{-- <h5>Auchan</h5> --}}
                    </div>
                    
                </div>
            </div>

            <div id="team">
                <h4 class="mb-4">Notre Equipe</h4>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet, velit corporis non aliquid facilis ipsum sint assumenda, ex dicta totam consectetur a perspiciatis sequi magni deserunt voluptatum eius rem quibusdam!
                </p>
                <div class="members mt-4">
                    <div class="member">
                        <img src="/image/team/richard.png" alt="">
                        <p class="name mt-4">Richard Hendricks</p>
                        <p class="profil">Founder & CEO</p>
                    </div>
                    <div class="member">
                        <img src="/image/team/richard.png" alt="">
                        <p class="name mt-4">Richard Hendricks</p>
                        <p class="profil">Founder & CEO</p>
                    </div>
                    <div class="member">
                        <img src="/image/team/richard.png" alt="">
                        <p class="name mt-4">Richard Hendricks</p>
                        <p class="profil">Founder & CEO</p>
                    </div>
                    <div class="member">
                        <img src="/image/team/richard.png" alt="">
                        <p class="name mt-4">Richard Hendricks</p>
                        <p class="profil">Founder & CEO</p>
                    </div>
                    <div class="member">
                        <img src="/image/team/richard.png" alt="">
                        <p class="name mt-4">Richard Hendricks</p>
                        <p class="profil">Founder & CEO</p>
                    </div>
                    <div class="member">
                        <img src="/image/team/richard.png" alt="">
                        <p class="name mt-4">Richard Hendricks</p>
                        <p class="profil">Founder & CEO</p>
                    </div>
                    <div class="member">
                        <img src="/image/team/richard.png" alt="">
                        <p class="name mt-4">Richard Hendricks</p>
                        <p class="profil">Founder & CEO</p>
                    </div>
                    <div class="member">
                        <img src="/image/team/richard.png" alt="">
                        <p class="name mt-4">Richard Hendricks</p>
                        <p class="profil">Founder & CEO</p>
                    </div>
                    
                </div>
            </div>


            <div id="contact">
                <h4 class="mb-4">Nous Contacter</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut, magni tempora quisquam fugiat nobis eaque rem culpa? Temporibus maxime repudiandae molestiae, voluptas perferendis magnam hic quod eveniet dicta ipsum! Eos!</p>
                <div class="socials mb-4">
                    <a href="#"> <i class="fab fa-facebook-square"></i> </a>
                    <a href="#"> <i class="fab fa-twitter-square"></i> </a>
                    <a href="#"> <i class="fab fa-linkedin"></i> </a>
                    <a href="#"> <i class="fab fa-youtube-square"></i> </a>
                </div>

                <div class="container">
                    <form action="#">
                        <div class="row">
                            <div class="form-group col-6">
                                <input type="text" placeholder="Entrez votre nom" class="form-control">
                            </div>
                            <div class="form-group col-6">
                                <input type="email" placeholder="Entrez votre email" class="form-control">
                            </div>
                            <div class="form-group col-12">
                                <textarea name="" class="form-control" id="" cols="30" rows="5" placeholder="Entrez votre message"></textarea>
                            </div>
                            <div class="form-group col-3 offset-4">
                                <input type="submit" value="Envoyer" class="btn btn-primary btn-block">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </main>

    {{-- end main --}}
    
    {{-- footer --}}
    
        <footer>
            <p>Copyright©{{ now()->year }} - {{ config('app.name') }} - Tous droits reserves</p>
        </footer>

    {{-- end footer --}}


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>