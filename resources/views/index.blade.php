<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">

    <title> {{ config('app.name') }} </title>

  {{-- Font awesome --}}
  <script src="https://kit.fontawesome.com/f794276008.js" crossorigin="anonymous"></script>
  </head>
  <body>
    {{-- Header --}}
      <nav class="navbar navbar-expand-lg navbar-light ">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
        <a class="navbar-brand" href="#">{{ config('app.name') }}</a>
        <ul class="navbar-nav mt-2 mt-lg-0">
          <li class="nav-item active">
            <a class="nav-link" href="#">Accueil <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Nous Sommes</a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="#">Mission</a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="#">Partenaires</a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="#">Contact</a>
          </li>
        </ul>

        <ul class="lang">
          <li>
            <a href="#">Fr <img src="{{ asset('/image/fr.png') }}" alt=""></a>
          </li>
          <li>
            <a href="#">En <img src="{{ asset('/image/en.png') }}" alt=""></a>
          </li><li>
            <a href="#">Es <img src="{{ asset('/image/es.svg') }}" alt=""></a>
          </li>
        </ul>
        {{-- <form class="form-inline my-2 my-lg-0">
          <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
          <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form> --}}
      </div>
    </nav>

      {{-- <p>Hello</p> --}}
   

    <div class="bd-example">
  <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
      <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner" >
      <div class="carousel-item active">
        <img src="/image/bg1.jpg" class="d-block w-100" alt="...">
        <div class="carousel-caption d-none d-md-block">
          <h5>First slide label</h5>
          <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
        </div>
      </div>
      <div class="carousel-item">
        <img src="https://www.lboutremer.com/wp-content/uploads/2017/09/TITRE-RESPONSABLE-DE-DEVELOPPEMENT-COMMERCIAL-Niveau-II-800x445.jpg" class="d-block w-100" alt="...">
        <div class="carousel-caption d-none d-md-block">
          <h5>Second slide label</h5>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
        </div>
      </div>
      <div class="carousel-item">
        <img src="/image/bg2.jpg" class="d-block w-100" alt="...">
        <div class="carousel-caption d-none d-md-block">
          <h5>Third slide label</h5>
          <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
        </div>
      </div>
    </div>
    <a class="carousel-control-prev" id="fleche" href="#carouselExampleCaptions" role="button" data-slide="prev">
      <span class="carousel-control-prev-icons" aria-hidden="true"><i class="fas fa-chevron-circle-left"></i></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" id="fleche" href="#carouselExampleCaptions" role="button" data-slide="next">
      <span class="carousel-control-next-icons" aria-hidden="true"><i class="fas fa-chevron-circle-right"></i></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>
{{-- end Header --}}

    {{-- main --}}

    <main class="container-fluid">
      <div id="about" class="text-center">
          <div class="container">
              <h4>Presentation</h4>

         <p>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident dignissimos unde dolorum accusamus. Obcaecati eum nam adipisci excepturi sapiente reiciendis dolor, alias ullam reprehenderit, illo quas eaque minima molestiae rerum.
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus laudantium officiis perferendis neque delectus ducimus amet iure, assumenda animi eius sapiente quis voluptates commodi quisquam exercitationem. Voluptas voluptatibus quae vel.
          </p>
          </div>
        {{-- <div class="row">
            <div class="col bg-light">
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident dignissimos unde dolorum accusamus. Obcaecati eum nam adipisci excepturi sapiente reiciendis dolor, alias ullam reprehenderit, illo quas eaque minima molestiae rerum.
              </p>
            </div>
            <div class="col">
                <img src="/image/logo.jpeg" alt="">
            </div>
        </div> --}}
      </div>

      <div id="mission">
          <div class="container text-center">
            <h4>Mission</h4>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos, repellendus dicta ad aut hic adipisci labore velit consequuntur quaerat commodi animi quibusdam inventore reiciendis, illum dolore enim est sit, obcaecati.
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus placeat voluptatem fugit ex quas alias sint velit explicabo. Doloribus animi explicabo facilis cupiditate quam culpa, fugiat. Neque nam aliquam adipisci!
          </p>

          <div class="row mt-5">
              <div class="col">
                <div class="card bg-success text-white">
                    {{-- <div class="header">
                      
                    </div> --}}
                    <h5>Mission 1</h5>
                    <p>
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum fugiat, dicta, voluptates corporis, magnam et qui delectus iusto iure recusandae, nulla cupiditate necessitatibus labore vel maiores quae aperiam repellendus ad!
                    </p>
                </div>
              </div>
              <div class="col">
                <div class="card">
                    {{-- <div class="header">
                      
                    </div> --}}
                    <h5>Mission 2</h5>
                    <p>
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum fugiat, dicta, voluptates corporis, magnam et qui delectus iusto iure recusandae, nulla cupiditate necessitatibus labore vel maiores quae aperiam repellendus ad!
                    </p>
                </div>
              </div>
              <div class="col">
                <div class="card bg-primary text-white">
                    {{-- <div class="header">
                      
                    </div> --}}
                    <h5>Mission 3</h5>
                    <p >
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum fugiat, dicta, voluptates corporis, magnam et qui delectus iusto iure recusandae, nulla cupiditate necessitatibus labore vel maiores quae aperiam repellendus ad!
                    </p>
                </div>
              </div>
              
          </div>
          </div>
      </div>

      <div id="partenaires">
          <h4>Nos Partenaires</h4>
          <div class="container text-center">
            <p>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat possimus asperiores inventore id atque doloribus laboriosam sint sed fugit ea est placeat repellat quidem, modi, esse distinctio ipsa repellendus nulla.
            </p>

            <div class="row">
                <div class="col my-3">
                    <div class="card" style="width: 18rem;">
                      <img src="/partner/auchan.jpg" class="card-img-top" alt="...">
                      <div class="card-body">
                        <a href="#" class="btn btn-success">Voir Plus</a>
                      </div>
                    </div>
                </div>
                <div class="col my-3">
                    <div class="card" style="width: 18rem;">
                      <img src="/partner/auchan.jpg" class="card-img-top" alt="...">
                      <div class="card-body">
                        <a href="#" class="btn btn-success">Voir Plus</a>
                      </div>
                    </div>
                </div>
                <div class="col my-3">
                    <div class="card" style="width: 18rem;">
                      <img src="/partner/auchan.jpg" class="card-img-top" alt="...">
                      <div class="card-body">
                        <a href="#" class="btn btn-success">Voir Plus</a>
                      </div>
                    </div>
                </div>
                <div class="col my-3">
                    <div class="card" style="width: 18rem;">
                      <img src="/partner/auchan.jpg" class="card-img-top" alt="...">
                      <div class="card-body">
                        <a href="#" class="btn btn-success">Voir Plus</a>
                      </div>
                    </div>
                </div>
                <div class="col my-3">
                    <div class="card" style="width: 18rem;">
                      <img src="/partner/auchan.jpg" class="card-img-top" alt="...">
                      <div class="card-body">
                        <a href="#" class="btn btn-success">Voir Plus</a>
                      </div>
                    </div>
                </div>
                <div class="col my-3">
                    <div class="card" style="width: 18rem;">
                      <img src="/partner/auchan.jpg" class="card-img-top" alt="...">
                      <div class="card-body">
                        <a href="#" class="btn btn-success">Voir Plus</a>
                      </div>
                    </div>
                </div>
                
            </div>
          </div>
      </div>

      <div class="team">
          <h4>Notre Equipe</h4>
          <div class="container">
              <div class="row">
                  <div class="col-md-3">
                      <div class="member text-center mb-5">
                          <img src="/image/team/richard.png" alt="" class="mb-4">
                          <p class="name">Richard Hendricks</p>
                          <p>Founder & CEO</p>
                      </div>
                  </div>

                  <div class="col-md-3">
                      <div class="member text-center mb-5">
                          <img src="/image/team/big-head.png" alt="" class="mb-4">
                          <p class="name">Grosse Tete</p>
                          <p>Majority Investor</p>
                      </div>
                  </div>
                  <div class="col-md-3">
                      <div class="member text-center mb-5">
                          <img src="/image/team/richard.png" alt="" class="mb-4">
                          <p class="name">Richard Hendricks</p>
                          <p>Founder & CEO</p>
                      </div>
                  </div>

                  <div class="col-md-3">
                      <div class="member text-center mb-5">
                          <img src="/image/team/big-head.png" alt="" class="mb-4">
                          <p class="name">Grosse Tete</p>
                          <p>Majority Investor</p>
                      </div>
                  </div>
                  <div class="col-md-3">
                      <div class="member text-center mb-5">
                          <img src="/image/team/richard.png" alt="" class="mb-4">
                          <p class="name">Richard Hendricks</p>
                          <p>Founder & CEO</p>
                      </div>
                  </div>

                  <div class="col-md-3">
                      <div class="member text-center mb-5">
                          <img src="/image/team/big-head.png" alt="" class="mb-4">
                          <p class="name">Grosse Tete</p>
                          <p>Majority Investor</p>
                      </div>
                  </div>
                  <div class="col-md-3">
                      <div class="member text-center mb-5">
                          <img src="/image/team/richard.png" alt="" class="mb-4">
                          <p class="name">Richard Hendricks</p>
                          <p>Founder & CEO</p>
                      </div>
                  </div>

                  <div class="col-md-3">
                      <div class="member text-center mb-5">
                          <img src="/image/team/big-head.png" alt="" class="mb-4">
                          <p class="name">Grosse Tete</p>
                          <p>Majority Investor</p>
                      </div>
                  </div>
                  
              </div>
          </div>
      </div>


      <div id="contact">
          <h4>Nous Contacter</h4>
          <div class="container text-center">
              <div class="social">
                  <a href="#"> <i class="fab fa-facebook-square"></i> </a>
                  <a href="#"> <i class="fab fa-twitter-square"></i> </a>
                  <a href="#"> <i class="fab fa-linkedin"></i> </a>
                  <a href="#"> <i class="fab fa-youtube-square"></i> </a>
              </div>

              <form action="">
                <div class="row">
                    <div class="form-group col">
                        <input type="text" placeholder="Votre nom" class="form-control">
                    </div>

                    <div class="form-group col">
                        <input type="email" placeholder="Votre email" class="form-control">
                    </div>

                    <div class="form-group col-12">
                        <textarea name="" id="" cols="5" rows="5" class="form-control" placeholder="Votre Message ici svp"></textarea>
                    </div>
                    <div class="form-group col-3 offset-4">
                        <input type="submit" class="btn btn-primary btn-block" value="ENVOYER">
                    </div>
                </div>
              </form>
          </div>
      </div>
    </main>

    {{-- end main --}}

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>